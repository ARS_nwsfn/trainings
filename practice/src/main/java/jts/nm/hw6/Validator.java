
package homework6;


public class Validator <T> {
    private T val;
    public Validator(){
        val = null;
    }
    public T getVal(){
        return val;
    }
    public void setVal(T val){
        this.val = val;
    }
    public void validate() throws ValidationFailedException{
        //throw new ValidationFailedException("not a real validator");
        if (val instanceof Integer){
            IntegerValidator.validate((Integer)val);
        } else if(val instanceof String){
            StringValidator.validate((String)val);
        } else throw new ValidationFailedException("wrong type or no data entered");
    }


}
