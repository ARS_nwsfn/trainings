
package homework6;


public class IntegerValidator /*extends Validator<Integer>*/ {
    public IntegerValidator(){
        super();
    }
    public static void validate(int val) throws ValidationFailedException{
        if ((val >= 1) && (val <= 10)){
            System.out.println("validation passed");
        }
        else {
            throw new ValidationFailedException("validation failed");
        }

    }
    
}
