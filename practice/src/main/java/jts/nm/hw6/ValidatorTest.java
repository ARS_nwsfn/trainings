/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ARS
 */
public class ValidatorTest {
    
    @Test
    public void testValidateInt() throws ValidationFailedException {
        System.out.println("testValidateInt");
        ValidationSystem instance = new ValidationSystem();
        instance.validate(1);
        instance.validate(5);
        instance.validate(10);
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidateIntFails() throws ValidationFailedException  {
        System.out.println("testValidateIntFails");
        ValidationSystem instance = new ValidationSystem();
        instance.validate(11);
    }
    @Test (expected = ValidationFailedException.class)
    public void testValidateIntFails2() throws ValidationFailedException  {
        System.out.println("testValidateIntFails2");
        ValidationSystem instance = new ValidationSystem();
        instance.validate(0);
    }
    @Test
    public void testValidateString () throws ValidationFailedException {
        System.out.println("testValidateString");
        ValidationSystem instance = new ValidationSystem();
        instance.validate("Hello");
        instance.validate("Hello world, abc");
        
    }
    @Test (expected = ValidationFailedException.class)
    public void testValidateStringFails() throws ValidationFailedException  {
        System.out.println("testValidateStringFails");
        ValidationSystem instance = new ValidationSystem();
        instance.validate("hello");
    }
    @Test (expected = ValidationFailedException.class)
    public void testValidateStringFails2() throws ValidationFailedException  {
        System.out.println("testValidateStringFails2");
        ValidationSystem instance = new ValidationSystem();
        instance.validate("");
    }

    
    
}
