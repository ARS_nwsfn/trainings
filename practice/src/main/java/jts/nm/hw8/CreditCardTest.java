
package homework8;

import org.junit.Test;
import static org.junit.Assert.*;


public class CreditCardTest {
    
    
    @Test
    public void testGetCash() {
        System.out.println("getCash");
        CreditCard instance = new CreditCard();
        int expResult = 500;
        int result = instance.getCash();
        assertEquals(expResult, result);
    }

    @Test
    public void testAddCash() {
        System.out.println("addCash");
        CreditCard instance = new CreditCard();
        for ( int i = 0; i < 10 ; i++){
            instance.addCash();
        }
        assertEquals(550,instance.getCash());
    }

    @Test
    public void testDivCash() {
        System.out.println("divCash");
        CreditCard instance = new CreditCard();
        for (int i = 0; i < 10; i++ ){
        instance.divCash();
        }
        assertEquals(450,instance.getCash());
    }
    
}
