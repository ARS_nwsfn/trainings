package homework8;

import java.util.Random;
import java.util.concurrent.ExecutorService;

public class Producer extends Thread {

    Thread threadPr;
    CreditCard acc;
    ExecutorService service;

    Producer(CreditCard currentAcc, ExecutorService service) {
        threadPr = new Thread(this, "produce thread");
        System.out.println("created " + threadPr.getName());
        threadPr.start();
        acc = currentAcc;
        this.service = service;
    }

    @Override
    public synchronized void run() {
        try {
            while (acc.getCash() < 550 && acc.getCash() > 450) {
                acc.addCash();
                if (acc.getCash() == 550) {
                    System.out.println("anough operations with cash for today");
                    service.shutdownNow();
                } else {
                    Thread.sleep(new Random(System.currentTimeMillis()).nextInt(400 - 10) + 10);
                }
            }
        } catch (InterruptedException e) {
            System.out.println(" produce thread interrupted");
        }
    }
}
