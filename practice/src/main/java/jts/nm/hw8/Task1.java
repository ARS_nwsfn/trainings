package homework8;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Task1 {

    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();
        CreditCard acc = new CreditCard(service);
        for (int i = 0; i < new Random(System.currentTimeMillis()).nextInt(5 - 3) + 3; i++) {
            service.execute(new Producer(acc, service));
        }
        for (int i = 0; i < new Random(System.currentTimeMillis()).nextInt(5 - 3) + 3; i++) {
            service.execute(new Consumer(acc, service));
        }
        service.shutdownNow();
    }

}
