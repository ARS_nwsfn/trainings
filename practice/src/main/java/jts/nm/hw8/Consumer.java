package homework8;

import java.util.Random;
import java.util.concurrent.ExecutorService;

public class Consumer extends Thread {

    Thread threadCon;
    CreditCard acc;
    ExecutorService service;

    Consumer(CreditCard currentAcc, ExecutorService service) {
        threadCon = new Thread(this, "consume thread");
        System.out.println("Created " + threadCon.getName());
        threadCon.start();
        acc = currentAcc;
        this.service = service;
    }

    @Override
    public synchronized void run() {
        try {
            while (acc.getCash() < 550 && acc.getCash() > 450) {
                acc.divCash();
                if (acc.getCash() == 450) {
                    System.out.println("anough operations with cash for today");
                    service.shutdownNow();
                } else {
                    Thread.sleep(new Random(System.currentTimeMillis()).nextInt(400 - 10) + 10);
                }
            }
        } catch (InterruptedException e) {
            System.out.println(" consume thread interrupted");
        }
    }
}
