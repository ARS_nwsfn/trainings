package homework8;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CreditCard {

    private int cash;
    ExecutorService service;

    public CreditCard() {
        cash = 500;
        service = Executors.newCachedThreadPool();
    }

    public CreditCard(ExecutorService service) {
        this.service = service;
        cash = 500;
    }

    public synchronized int getCash() {
        return cash;
    }

    public synchronized void addCash() {
        int rand = new Random(System.currentTimeMillis()).nextInt(10 - 5) + 5;
        cash += rand;
        if (cash < 550) {
            System.out.println(rand + "$ produced, current cash is " + getCash());
        } else {
            System.out.println("upper limit is 550$, produced only  " + (rand - (getCash() - 550)));
            cash = 550;
            service.shutdownNow();
        }

    }

    public synchronized void divCash() {
        int rand = new Random(System.currentTimeMillis()).nextInt(10 - 5) + 5;
        cash -= rand;
        if (cash > 450) {
            System.out.println(rand + "$ consumed, current cash is " + getCash());
        } else {
            System.out.println("bottom limit is 450$, consumed only  " + (rand - (450 - getCash())));
            cash = 450;
            service.shutdownNow();
        }
    }
}
