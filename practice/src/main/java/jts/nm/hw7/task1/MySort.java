package homework7;

import java.util.*;

public class MySort {

    protected ArrayList<Integer> copy = new ArrayList<>();

    public ArrayList<Integer> sort(ArrayList<Integer> array) {
        if (array.size() == 0) {
            throw new NullPointerException("array is empty");
        }
        for (int item : array) {                  // here it creates a copy of array that contains
            int temp = 0;                      // sum of numbers of every item in array
            while (item > 0) {
                temp += item % 10;
                item /= 10;
            }
            copy.add(temp);
            System.out.print(temp + " ");
        }
        System.out.println("\n ---^-copy----");
        for (int i = array.size() - 1; i > 0; i--) {         // here it sorts array "copy"
            for (int j = 0; j < i; j++) {
                if (copy.get(j) > copy.get(j + 1)) {
                    int tmp = copy.get(j);
                    copy.set(j, copy.get(j + 1));          // but if it changes something in "copy"
                    copy.set(j + 1, tmp);
                    tmp = array.get(j);
                    array.set(j, array.get(j + 1));    // it does the same in "array"
                    array.set(j + 1, tmp);

                }
            }
        }
        return array;
    }
}
