package homework7;

import java.util.*;

public class Task1 {

    public static void main(String[] args) {
        ArrayList<Integer> array = new ArrayList<>();
        MySort mySort = new MySort();
        for (int i = 0; i < 10; i++) {
            array.add((int) (Math.random() * 155) + 15);
            System.out.print(array.get(i) + " ");
        }
        System.out.println("\n ----our array----");
        mySort.sort(array);
        for (int i = 0; i < 10; i++) {
            System.out.print(array.get(i) + " ");
        }
        System.out.println("\n --^--sorted----");
    }

}
