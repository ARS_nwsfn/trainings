package homework7_2;

import java.util.HashSet;
import org.junit.Test;

public class MyMathTest {

    HashSet<Integer> mySet1 = new HashSet<>();
    HashSet<Integer> mySet2 = new HashSet<>();

    {
        mySet1.add(1);
        mySet1.add(2);
        mySet1.add(3);

        mySet2.add(2);
        mySet2.add(3);
        mySet2.add(4);
    }

    @Test(expected = NullPointerException.class)
    public void testPrintFailed() {
        System.out.println("print");
        MyMath.print(null);
    }

    @Test
    public void testPrint2() {
        System.out.println("print");
        HashSet<Integer> mySet = new HashSet<>();
        mySet.add(1);
        mySet.add(2);
        MyMath.print(mySet);
    }

    @Test(expected = NullPointerException.class)
    public void testUnionFailed() {
        System.out.println("union failed");
        MyMath.union(null, null);
    }

    @Test
    public void testUnion() {
        System.out.println("union");
        MyMath.union(mySet1, mySet2);
    }

    @Test(expected = NullPointerException.class)
    public void testIntersectionFailed() {
        System.out.println("intersection Failed");
        MyMath.intersection(null, null);
    }

    @Test
    public void testIntersection() {
        System.out.println("intersection");
        MyMath.intersection(mySet1, mySet2);
    }

    @Test(expected = NullPointerException.class)
    public void testDifferenceFailed() {
        System.out.println("difference Failed");
        MyMath.difference(null, null);
    }

    @Test
    public void testDifference() {
        System.out.println("difference");
        MyMath.difference(mySet1, mySet2);
    }

    @Test(expected = NullPointerException.class)
    public void testExclusionFailed() {
        System.out.println("exclusion Failed");
        MyMath.exclusion(null, null);
    }

    @Test
    public void testExclusion() {
        System.out.println("exclusion Failed");
        MyMath.exclusion(mySet1, mySet2);
    }

}
