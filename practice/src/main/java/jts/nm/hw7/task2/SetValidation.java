package homework7_2;

import java.util.HashSet;

public class SetValidation {

    public static <T> void setValidation(HashSet<T> set) {
        if (set.isEmpty()) {
            throw new NullPointerException("There is no data in set");
        }

    }

    public static <T> void setValidation(HashSet<T> set1, HashSet<T> set2) {
        if (set1.isEmpty() || set2.isEmpty()) {
            throw new NullPointerException("There is no data in one or both of the sets");
        }
    }
}
