package homework7_2;

import java.util.*;

public class MyMath {

    public static <T> void print(HashSet<T> set) {
        SetValidation.setValidation(set);
        for (T item : set) {
            System.out.print(item + " ");
        }
        System.out.println("\n-------");
    }

    public static <T> void union(HashSet<T> set1, HashSet<T> set2) {
        SetValidation.setValidation(set1, set2);
        HashSet<T> tempSet = new HashSet<>();               //this could have been done with just adding item from
        for (T item : set1) {                                //set2 into set1, becouse add in HashSet checks if set already
            tempSet.add(item);                             //contains what you are trying to add
        }                                                 //but i wanted this method not to change origin sets
        for (T item : set2) {
            tempSet.add(item);
        }
        System.out.print("union: ");
        print(tempSet);
    }

    public static <T> void intersection(HashSet<T> set1, HashSet<T> set2) {
        SetValidation.setValidation(set1, set2);
        HashSet<T> tempSet = new HashSet<>();
        for (T item : set1) {
            if (set2.contains(item)) {
                tempSet.add(item);
            }
        }
        System.out.print("intersection: ");
        print(tempSet);
    }

    public static <T> void difference(HashSet<T> set1, HashSet<T> set2) {
        SetValidation.setValidation(set1, set2);
        HashSet<T> tempSet = new HashSet<>();
        for (T item : set1) {
            if (!set2.contains(item)) {
                tempSet.add(item);
            }
        }
        System.out.print("difference: ");
        print(tempSet);
    }

    public static <T> void exclusion(HashSet<T> set1, HashSet<T> set2) {
        SetValidation.setValidation(set1, set2);
        HashSet<T> tempSet = new HashSet<>();
        for (T item : set1) {
            if (!set2.contains(item)) {
                tempSet.add(item);
            }
        }
        for (T item : set2) {
            if (!set1.contains(item)) {
                tempSet.add(item);
            }
        }
        System.out.print("exclusion: ");
        print(tempSet);
    }
}
