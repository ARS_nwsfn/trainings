
package homework4;

public class PrintNode {
    static void printNode(Folder node,String offset) {
        try{
        if (node == null){
            throw new NullPointerException("node not found");
        }
        }catch (NullPointerException e){
            System.out.println("Node not found");
            return;
        }
        System.out.println(" " + offset + node.getName() + "/");
        offset+="   ";
        if(!("File".equals(node.getClass().getName()))){            //if current item of Folder ArrayList is not File
            for(Folder child : node.get() ){
                printNode(child,offset);
            }
            
        }
        
            
    }
}
