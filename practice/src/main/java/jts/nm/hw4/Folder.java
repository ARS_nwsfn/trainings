package homework4;
import  java.util.ArrayList;

public class Folder {
    
    private final ArrayList<Folder> children = new ArrayList<>();
    private String name; 
    public Folder(String name){
        this.name = name;
    }
    public Folder(){
        this.name = "NoName";
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public void add(Folder child){
        try{
        children.add(child);
        } catch (NullPointerException e){
            System.out.println("there is no data to add");
        }
    }
    public ArrayList<Folder> get()
    {
       
            if (children.size() != 0){
            return children;
            } else return null;

    }
    public boolean containsChild(String name){
        boolean temp = false;
        for(Folder child : children ){
            if (child.getName().equals(name)){
               temp = true;
               break;
            }
                
        }
        return temp;
    }
    
    
}
