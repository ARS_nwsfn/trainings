
package homework5;
import java.util.Arrays;
import  java.util.List;
import  java.util.*;
public class Task1 {


    public static void main(String[] args) {   
        String text = "Imperial force defied, " +
                      "Facing five hundred samurai " +
                      "Surrounded and outnumbered, " +
                      "sixty to 1 the sword face the gun " +
                      "Bushido dignified, " +
                      "Its the last stand of the samurai " +
                      "Surrounded and outnumbered";
        List<String> parts = new ArrayList<>();
        List<String> partsNew = new ArrayList<>();
        text = text.replaceAll("[-.?!)(,:]","").toLowerCase();    //deleting punctuation and getting all string to lower case;
        text = text.replaceAll("(?<=^|\\s)[0-9]+(?=$|\\s)", ""); // deleting numbers from string;
        text = text.replaceAll("[\\s]{2,}", " ");               // deleting extra spaces in text;
        System.out.println(text);                              // string after all convertations;
        parts.addAll(Arrays.asList(text.split(" "))); 
        Collections.sort(parts);
        for (String part : parts){
            if (!partsNew.contains(part)){
                partsNew.add(part);
            }
        } 
        String[] myStr;
        char alph = 'a';
        System.out.print(alph + ": ");
        for (String part : partsNew){              //  for each item in array;
            myStr = part.split("");
            while(true){
            if (myStr[0].equals(String.valueOf(alph))){       //if first letter equals to a variable alph(alphabet);
            System.out.print(part + " ");                    //we print it;
            break;
            }
            else {               
                System.out.print("\n" + ++alph + ": ");     // or proceed to the next letter;
            }
            }
        } 
        System.out.println();
    }
    
}
