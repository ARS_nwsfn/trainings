
package task3;

public class FibonachiCheck
    {
        int counter = 1;
        double first = 1d;
        double second = 1d;
        double temp;
        public void reset()
                {
                   counter = 1; 
                   first = 1d;
                   second = 1d;
                }
        
        public void fibonachiIntCheck()
        {
            if ((first +second) < Integer.MAX_VALUE) 
            {                                                             
                temp = second + first;                     
                first = second;
                second = temp;
                counter++;
                fibonachiIntCheck();
            }
            else 
            {
                System.out.println("Max value of the fibonachi in range of int is " + counter);
                reset();
            }
        }
        public void fibonachiLongCheck()
        {
            if ((first +second) < Long.MAX_VALUE)
            {                                   
                temp = second + first;             
                first = second;
                second = temp;
                counter++;
                fibonachiLongCheck();
            }
            else
            {
                System.out.println("Max value of the fibonachi in range of Long is " + counter);
                reset();
            }
        }
    }
