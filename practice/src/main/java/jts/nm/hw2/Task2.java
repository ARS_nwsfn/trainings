
public class Task2 {
    int var = checkLoad("common variable");
        static int staticVar = checkLoad("static variable");
        static { checkLoad("static block one");          
        }
        static { checkLoad("static block two");          
        }
        {
            checkLoad("nonStatic block one");
        }
        {
            checkLoad("nonStatic block two");
        }
        static int checkLoad(String message)
        {
            System.out.println(message + " is loaded");
            return 0;
        }

   
    public static void main(String[] args) {
       System.out.println("Program is started, object not created");
       System.out.println("Creating reference type variable");
       Task2 que;
       System.out.println("variable is assigned to an object ");
       que = new Task2();
       System.out.println("program completed");
        
    }
    
}
