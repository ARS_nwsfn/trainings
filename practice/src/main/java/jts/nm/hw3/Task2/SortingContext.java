package homework3_2;


public class SortingContext 
{
    private Sorter sorter;
    public SortingContext(){};    
    public void setSort(Sorter sorter)
    {
        this.sorter = sorter;
    }
    public void execSort(int[] array)
    {
        sorter.sort(array);
    }
    
}
    

