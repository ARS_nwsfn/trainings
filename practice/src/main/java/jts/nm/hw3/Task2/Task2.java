
package homework3_2;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Task2 {
    


    public static void main(String[] args) throws IOException {
      
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      SortingContext context = new SortingContext();
      int[] array = new int[10];
        for (int i = 0; i<10;i++) 
        {
            array[i] = ((int)(Math.random() * 31) - 15);
            System.out.print(array[i] + " ");
        }  
      System.out.println("\n Choose sorting type by pressing "
      + "\n 1: Bubblesort \n 2: for SelectionSort \n anything for default sort: \n");
      switch (br.readLine())
      {
          case "1": 
          {
            context.setSort(new BubbleSort());
            context.execSort(array);
          }break;
          case "2":
          {
            context.setSort(new SelectionSort());
            context.execSort(array);
          }break;
          default:
          {
              System.out.println("if you can't choose even a number, i won't sort anything");
          } break;
          
      }
      
      System.out.println("array after sort:");
      for (int i = 0; i<10;i++) 
        {
            System.out.print(array[i] + " ");
        } 
      
    }
    
}
