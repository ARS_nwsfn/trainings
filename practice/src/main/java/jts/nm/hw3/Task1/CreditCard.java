package homework4;

public class CreditCard extends Card {

    @Override
    void withdraw(int n)
    {
        if ((cash-n) < 0)
            System.out.println("Not enough money on a card");
        else setCash(cash-n);
            
    }
    
}
