package homework4;


public class DebitCard extends Card {

    @Override
    int getCash() 
    {
        if (cash>0)
            return super.getCash();
        else 
        {
            System.out.println("your debts are: " + Math.abs(cash) + "$");
            return Math.abs(cash);
        }
    }
    
}