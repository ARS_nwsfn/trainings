
package homework4;


public class CardTest {
    
  void withdrawTest(Card card)
  {
      card.setCash(10);
      card.withdraw(10);
      if (card.getCash()==0)
          System.out.println("Method withdraw from class 'card' is ok");
      else System.out.println("Method withdraw from class 'card' is not ok");     
  }
  void depositTest(Card card)
  {
      card.setCash(0);
      card.deposit(5);
      if (card.getCash()==5)
          System.out.println("Method deposit from class 'card' is ok");
      else System.out.println("Method deposit from class 'card' is not ok");   
  }

}
