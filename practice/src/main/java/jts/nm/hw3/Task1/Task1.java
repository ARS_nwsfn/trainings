package homework4;



public class Task1 {


    public static void main(String[] args) {
        
        Card defCard = new Card();
        DebitCard debtCard = new DebitCard();
        CreditCard CredCard = new CreditCard();
        Atm atm = new Atm();
        atm.withdraw(debtCard, 26);
        atm.deposit(debtCard, 10);
        atm.getCash(debtCard);
        atm.deposit(CredCard, 10);
        atm.withdraw(CredCard, 5);
        atm.getCash(CredCard);
        CardTest test = new CardTest();
        test.withdrawTest(defCard);
        test.depositTest(defCard);
    }
    
}
