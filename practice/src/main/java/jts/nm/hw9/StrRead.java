package homework4;


public class StrRead {
    private String[] parts;
    public StrRead(Folder root, String str){
        
        parts = str.split("/");
        Folder currentNode = root;
        for ( int i = 0; i < parts.length; i++){
            if (!currentNode.containsChild(parts[i])){
                if (parts[i].contains(".")){     //file check
                    currentNode.add(new File(parts[i]));                    
                }
                else {
                    currentNode.add(new Folder(parts[i]));
                }
            }
            
            currentNode = MyFirst.first(currentNode,parts[i]);
        }
    }
    
}
