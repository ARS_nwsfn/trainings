package homework4;

import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class InSerializeTest {
    

    @Test
    public void testGetRoot() throws IOException, ClassNotFoundException {   //this test will throw an error if you already saved some object.ser
        System.out.println("getRoot");                                      // while there is no object.ser it would not load anything
        InSerialize instance = new InSerialize();                          // but once you save it, program will load it in InSerialize
        Folder expResult = null;                                          // BUT test will still expect NULL
        Folder result = instance.getRoot();
        assertEquals(expResult, result);
    }
    
}
