package homework4;

import java.io.*;
import java.util.*;

public class OutSerialize {
    public  OutSerialize(Folder root) throws IOException{
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("objects.ser"));
        Date now = new Date(System.currentTimeMillis());
        out.writeObject(now);
        out.writeObject(root);
        out.close();
        System.out.println("I have written some data");
        System.out.println("A Date object: "+now);
    }

}
