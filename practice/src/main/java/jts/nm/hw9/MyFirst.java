
package homework4;
/*

This class is an analog to a C# function List.First for java ArrayList
It returns first item from list that meet the requirements.
The requrement here is : having field name equals to a variable

*/

import java.util.ArrayList;


public class MyFirst {
    public static Folder first(Folder currentNode, String part){   ///first is an analogue to List.First from c#
                                                           /// it returns first item from array whose name equals to our parameter
        ArrayList<Folder> tmpArr = currentNode.get();
        Folder tmpFold = null;                                                   
        for (int i = 0; i < tmpArr.size(); i++){
            if (tmpArr.get(i).getName().equals(part)){
                tmpFold = tmpArr.get(i);
            }
        }
        if (tmpFold == null) throw new NullPointerException("There is no such folder");
        return tmpFold;
    }
}
