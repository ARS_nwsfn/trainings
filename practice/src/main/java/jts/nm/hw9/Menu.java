package homework4;

import java.io.IOException;
import java.util.Scanner;

public class Menu {

    Folder root = new Folder("root");

    public void menu() throws IOException, ClassNotFoundException {

        System.out.println("start");
        Scanner sc = new Scanner(System.in);
        String temp = "";
        System.out.println("Enter new folder / write 'print' to show current folders / 'end' to stop");
        while (temp != "end") {
            System.out.println("waiting command...");
            temp = sc.nextLine();
                if (temp.length() == 0) {
                    throw new NullPointerException("There is no data");
                }
            switch (temp) {
                case "print": {
                    PrintNode.printNode(root, "");
                } break;
                case "read": {
                    root = new InSerialize().getRoot();
                }
                break;
                case "end": {
                    temp = "end";
                    new OutSerialize(root);
                }
                break;
                default: {
                    new StrRead(root, temp);
                }
                break;
            }

        }
    }

}
