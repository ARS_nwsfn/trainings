package homework4;

import org.junit.Test;
import static org.junit.Assert.*;

public class PrintNodeTest {
    
    public PrintNodeTest() {
    }
    
    @Test (expected = NullPointerException.class)
    public void testPrintNode() {
        System.out.println("printNode");
        Folder node = null;
        String offset = "";
        PrintNode.printNode(node, offset);
    }
    
}
