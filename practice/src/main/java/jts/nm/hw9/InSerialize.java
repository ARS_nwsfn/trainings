package homework4;

import java.io.*;
import java.util.*;
import java.io.File;

public class InSerialize {
    static Folder root;
    public InSerialize() throws IOException, ClassNotFoundException {
        if (new File("objects.ser").exists()) {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("objects.ser"));
            Date date = (Date) in.readObject();
            this.root = (Folder) in.readObject();
            System.out.println("I have read some data");
            System.out.println("A Date object: " + date);
        } else {
            System.out.println("nothing to load");
        }
    }
    public Folder getRoot(){
        return root;
    }

}
