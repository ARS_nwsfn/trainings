package homework4;

public class PrintNode {

    static void printNode(Folder node, String offset) {
            if (node == null) {
                throw new NullPointerException("node not found");
            }
        System.out.println(" " + offset + node.getName() + "/");
        offset += "   ";
        if (!("File".equals(node.getClass().getName()))) {            //if current item of Folder ArrayList is not File
            for (Folder child : node.get()) {
                printNode(child, offset);
            }

        }
    }
}
