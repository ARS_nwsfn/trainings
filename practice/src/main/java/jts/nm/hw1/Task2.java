public class Task2 {
     public static void usingWhile(int algoritmId,int n)
    {
        int first = 0,second = 1;
        int counter = 0, result;
        switch (algoritmId)
        {
            case 1:
            {
                System.out.print("Fibonacci using While: 1 ");
                while (counter++ < n-1)
                {
                    result =first + second;
                    first = second;
                    second = result;
                    System.out.print(result + " ");                   
                }               
            } break;
            case 2:
            {
                result = n;
                while (n>1)
                {
                    n--;
                    result = result * n;  
                }
                System.out.println("Factorial using While: " + result);
            } break;
            default: 
                System.out.println("wrong algoritmId"); break;
        }
    }
    public static void usingDoWhile(int algoritmId,int n)
    {
        int first = 0,second = 1;
        int counter = 0, result;
        switch (algoritmId)
        {
            case 1:
            {
                System.out.print("Fibonacci using DoWhile: 1 ");
                do
                {
                    counter++;
                    result =first + second;
                    first = second;
                    second = result;
                    System.out.print(result + " ");   
                } while(counter < n-1);
            } break;
            case 2: 
            {
                result = n;
                do
                {
                    n--;
                    result=result * n;
                } while (n>1);
                System.out.println("Factorial using DoWhile: " + result);
            } break;
            default: 
                System.out.println("wrong algoritmId"); break;
        }
    }
    public static void usingFor(int algoritmId,int n)
    {
        int first = 0,second = 1;
        int counter = 0, result;
        switch (algoritmId)
        {
            case 1:
            {
                System.out.print("Fibonacci using For: 1 ");
                for(;counter < n-1; counter++)
                {
                    result = first + second;
                    first = second;
                    second = result;
                    System.out.print(result + " ");   
                }
                
            } break;
            case 2:
            {
                result = n;
                for(n-- ;n > 1;n--)
                {
                    result = result * n;
                }
                System.out.println("Factorial using For: " + result);
            } break;
            default: 
                System.out.println("wrong algoritmId"); break;
        }
        
    }
    


    public static void main(String[] args) 
    {
         if (args.length>=2)
	{
            
     	   int algoritmId = Integer.parseInt(args[0]);   
       	   int loopType = Integer.parseInt(args[1]);  
       	   int n = Integer.parseInt(args[2]);
           switch (loopType)
           {
               case 1:
               {
                   usingWhile(algoritmId,n);
               } break;
               case 2:
               {
                   usingDoWhile(algoritmId,n);
               } break;
               case 3:
               {
                   usingFor(algoritmId,n);
               } break;
               default: System.out.println("Wrong loop number"); break;
           } 
           
	}
        else System.out.println("Wrong number of arguments");
    }
    
}

