import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) 
    {
        if (args.length>=3)
	{
     	   int a = Integer.parseInt(args[0]);   
       	   int p = Integer.parseInt(args[1]);  
       	   double m1 = Double.parseDouble(args[2]);
       	   double m2 = Double.parseDouble(args[3]);
       	   double numerator,denumerator;
      	   Double G;
        	   numerator = 4. * Math.pow(Math.PI, 2) * Math.pow(a, 3);
       	   denumerator = Math.pow(p,2) * (m1 + m2);
        	   G = numerator / denumerator;
        	   System.out.println("G = " + G);
	}
        else System.out.println("Wrong number of arguments");

    }
    
}
